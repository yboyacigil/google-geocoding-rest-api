README
===

A JSON RESTful API fetching data from Google Geocoding service in XML format for a given address.

Used Spring Boot and Apache Camel.

# Approach

[Spring Boot](https://projects.spring.io/spring-boot/) has been used mainly for dependency injection and application startup.

The main part of the application uses [Apache Camel](http://camel.apache.org) to:

1. Create REST endpoint using `camel-rest` REST DSL,  
2. Caching using `camel-ehcache`
3. Camel processor to fetch XML data Google Geocoding service.

The main flow is as following:

1. We get a request for geocode data for a given address
2. Try to get it from an cache using `address` as the key
3. If we fail to get data from cache then use the procesor `GeoCodeFetcher` to fetch XML data from Google Geocoding service and cache this data.
4. Eventually XML data is parsed into `GeocodingResponse` instance which is then converted into JSON send sent as a response to initial request.

**Comments/notes about the implementation:**

- **XPath to help**: Used xpath to parse `GeocodingResponse` in the XML data from Google Geocoding service since only needed to return formatted address, latitude and longtitude.

- **Caching parsed response**: Tried to cache parsed response but that simply yielded type conversion errors which i expected Camel will handle through `RestBindingMode.json`. However when parsed at the end of the pipe line conversion did work as expected. Did not want to create a new type converter for that, because automatic conversion to JSON is already there in the REST route configuration. But could not figure out what is causing the error, either.

- **Using camel-http**: Tried to make use of `camel-http` component to fetch XML data to Google Geocoding service as a processor in the pipe line on cache-miss, but got errors such that I should have configured it as a bridgeEnpoint. I could not make it happen even configured so. Then decided to implement a processor myself.

- **Lack of right answers in the Internet**: Getting information to camel related issues without reading most of the documentation was the most difficult part. Most of the time failed to find solutions to specific problems.

# Build & Run

To build and run the application in the project root folder:

```
./mvnw spring-boot:run
```

This will create the rest endpoint at address: 0.0.0.0 and port 8080

To get geocode for a specified address:

```
curl -X GET http://0.0.0.0:8080/geocode\?address\=Backbase

{"status":"OK","results":[{"formattedAddress":"10 10th St NE, Atlanta, GA 30309, USA","latitude":"33.7814924","longtitude":"-84.3875728"}]}
```
