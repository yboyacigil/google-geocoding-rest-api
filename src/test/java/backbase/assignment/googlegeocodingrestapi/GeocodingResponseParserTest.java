package backbase.assignment.googlegeocodingrestapi;

import backbase.assignment.googlegeocodingrestapi.model.GeocodingResponse;
import backbase.assignment.googlegeocodingrestapi.model.GeocodingResult;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class GeocodingResponseParserTest {

    private static final String VALID_XML = "<GeocodeResponse>\n" +
            " <status>OK</status>\n" +
            " <result>\n" +
            "  <type>premise</type>\n" +
            "  <formatted_address>Google Bldg 41, 1600 Amphitheatre Pkwy, Mountain View, CA 94043, USA</formatted_address>\n" +
            "  <address_component>\n" +
            "   <long_name>Google Building 41</long_name>\n" +
            "   <short_name>Google Bldg 41</short_name>\n" +
            "   <type>premise</type>\n" +
            "  </address_component>\n" +
            "  <address_component>\n" +
            "   <long_name>1600</long_name>\n" +
            "   <short_name>1600</short_name>\n" +
            "   <type>street_number</type>\n" +
            "  </address_component>\n" +
            "  <address_component>\n" +
            "   <long_name>Amphitheatre Parkway</long_name>\n" +
            "   <short_name>Amphitheatre Pkwy</short_name>\n" +
            "   <type>route</type>\n" +
            "  </address_component>\n" +
            "  <address_component>\n" +
            "   <long_name>Mountain View</long_name>\n" +
            "   <short_name>Mountain View</short_name>\n" +
            "   <type>locality</type>\n" +
            "   <type>political</type>\n" +
            "  </address_component>\n" +
            "  <address_component>\n" +
            "   <long_name>Santa Clara County</long_name>\n" +
            "   <short_name>Santa Clara County</short_name>\n" +
            "   <type>administrative_area_level_2</type>\n" +
            "   <type>political</type>\n" +
            "  </address_component>\n" +
            "  <address_component>\n" +
            "   <long_name>California</long_name>\n" +
            "   <short_name>CA</short_name>\n" +
            "   <type>administrative_area_level_1</type>\n" +
            "   <type>political</type>\n" +
            "  </address_component>\n" +
            "  <address_component>\n" +
            "   <long_name>United States</long_name>\n" +
            "   <short_name>US</short_name>\n" +
            "   <type>country</type>\n" +
            "   <type>political</type>\n" +
            "  </address_component>\n" +
            "  <address_component>\n" +
            "   <long_name>94043</long_name>\n" +
            "   <short_name>94043</short_name>\n" +
            "   <type>postal_code</type>\n" +
            "  </address_component>\n" +
            "  <geometry>\n" +
            "   <location>\n" +
            "    <lat>37.4224082</lat>\n" +
            "    <lng>-122.0856086</lng>\n" +
            "   </location>\n" +
            "   <location_type>ROOFTOP</location_type>\n" +
            "   <viewport>\n" +
            "    <southwest>\n" +
            "     <lat>37.4211404</lat>\n" +
            "     <lng>-122.0869189</lng>\n" +
            "    </southwest>\n" +
            "    <northeast>\n" +
            "     <lat>37.4238383</lat>\n" +
            "     <lng>-122.0842209</lng>\n" +
            "    </northeast>\n" +
            "   </viewport>\n" +
            "   <bounds>\n" +
            "    <southwest>\n" +
            "     <lat>37.4221145</lat>\n" +
            "     <lng>-122.0859841</lng>\n" +
            "    </southwest>\n" +
            "    <northeast>\n" +
            "     <lat>37.4228642</lat>\n" +
            "     <lng>-122.0851557</lng>\n" +
            "    </northeast>\n" +
            "   </bounds>\n" +
            "  </geometry>\n" +
            "  <place_id>ChIJxQvW8wK6j4AR3ukttGy3w2s</place_id>\n" +
            " </result>\n" +
            "</GeocodeResponse>";

    private static final String NOT_OK_XML = "<GeocodeResponse>\n" +
            " <status>NOT_OK</status>\n" +
            "</GeocodeResponse>";

    private static final String MULTIPLE_RESULTS_XML = "<GeocodeResponse>\n" +
            " <status>OK</status>\n" +
            " <result>\n" +
            "  <formatted_address>formatted_address_1</formatted_address>\n" +
            "  <geometry>\n" +
            "   <location>\n" +
            "    <lat>lat_1</lat>\n" +
            "    <lng>lng_1</lng>\n" +
            "   </location>\n" +
            "  </geometry>\n" +
            " </result>\n" +
            " <result>\n" +
            "  <formatted_address>formatted_address_2</formatted_address>\n" +
            "  <geometry>\n" +
            "   <location>\n" +
            "    <lat>lat_2</lat>\n" +
            "    <lng>lng_2</lng>\n" +
            "   </location>\n" +
            "  </geometry>\n" +
            " </result>\n" +
            "</GeocodeResponse>";

    @Test
    public void parseValidResponse() {

        GeocodingResponse response = new GeocodingResponseParser().parse(VALID_XML);

        assertNotNull(response);
        assertEquals("OK", response.getStatus());
        assertNotNull(response.getResults());
        assertEquals(1, response.getResults().size());

        List<GeocodingResult> results = response.getResults();
        assertNotNull(results.get(0));
        assertEquals("Google Bldg 41, 1600 Amphitheatre Pkwy, Mountain View, CA 94043, USA", results.get(0).getFormattedAddress());
        assertEquals("37.4224082", results.get(0).getLatitude());
        assertEquals("-122.0856086", results.get(0).getLongtitude());
    }

    @Test
    public void parseNotOkStatus() {
        GeocodingResponse response = new GeocodingResponseParser().parse(NOT_OK_XML);

        assertNotNull(response);
        assertEquals("NOT_OK", response.getStatus());
        assertNull(response.getResults());
    }

    @Test
    public void parseMultipleResults() {
        GeocodingResponse response = new GeocodingResponseParser().parse(MULTIPLE_RESULTS_XML);

        assertNotNull(response);
        assertEquals("OK", response.getStatus());
        assertNotNull(response.getResults());
        assertEquals(2, response.getResults().size());

        List<GeocodingResult> results = response.getResults();
        assertNotNull(results.get(0));
        assertEquals("formatted_address_1", results.get(0).getFormattedAddress());
        assertEquals("lat_1", results.get(0).getLatitude());
        assertEquals("lng_1", results.get(0).getLongtitude());

        assertNotNull(results.get(1));
        assertEquals("formatted_address_2", results.get(1).getFormattedAddress());
        assertEquals("lat_2", results.get(1).getLatitude());
        assertEquals("lng_2", results.get(1).getLongtitude());

    }

}