package backbase.assignment.googlegeocodingrestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GoogleGeocodingRestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(GoogleGeocodingRestApiApplication.class, args);
	}
}
