package backbase.assignment.googlegeocodingrestapi;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.net.URLEncoder;

@Component("GeocodeFetcher")
public class GeocodeFetcher implements Processor {
    private static final Logger logger = LoggerFactory.getLogger(GeocodeFetcher.class);

    private static final String FETCH_FAILED = "<GeocodeResponse>\n" +
            " <status>FETCH_FAILED</status>\n" +
            "</GeocodeResponse>";

    @Value("${geocoding.apiUrl}")
    private String apiUrl;

    @Value("${geocoding.apiKey}")
    private String apiKey;

    @Override
    public void process(Exchange exchange) throws Exception {
        String address = (String) exchange.getIn().getHeader("address");

        String xml = FETCH_FAILED;

        DefaultHttpClient httpClient = new DefaultHttpClient();
        try {
            HttpGet httpGet = new HttpGet(apiUrl + "?address=" + URLEncoder.encode(address, "utf-8") + "&key=" + apiKey);
            httpGet.addHeader("accept", "application/xml");

            HttpResponse httpResponse = httpClient.execute(httpGet);
            if (httpResponse.getStatusLine().getStatusCode() == 200) {
                HttpEntity httpEntity = httpResponse.getEntity();
                xml = EntityUtils.toString(httpEntity);
            }
        } catch (Exception e) {
            logger.error("Error in fetching geocode", e);
        } finally {
            httpClient.getConnectionManager().shutdown();
        }

        exchange.getOut().setHeader("address", address);
        exchange.getOut().setBody(xml);
    }
}
