package backbase.assignment.googlegeocodingrestapi;

import backbase.assignment.googlegeocodingrestapi.model.GeocodingResponse;
import backbase.assignment.googlegeocodingrestapi.model.GeocodingResult;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

@Component
public class GeocodingResponseParser {
    private XPathFactory factory = XPathFactory.newInstance();

    public GeocodingResponse parse(String xml) {
        GeocodingResponse response = new GeocodingResponse();

        XPath xpath = factory.newXPath();
        try {
            String status = (String) xpath.evaluate("/GeocodeResponse/status/text()", new InputSource(new StringReader(xml)), XPathConstants.STRING);
            if (StringUtils.isEmpty(status)) {
                throw new IllegalArgumentException("No status in response");
            }
            response.setStatus(status);

            if ("OK".equals(status)) {
                List<GeocodingResult> geocodingResults = new ArrayList<>();
                NodeList nodes = (NodeList) xpath.evaluate("/GeocodeResponse/result", new InputSource(new StringReader(xml)), XPathConstants.NODESET);
                for (int i=0; i < nodes.getLength(); i++) {
                    Node resultNode = nodes.item(i);

                    GeocodingResult geocodingResult = new GeocodingResult();

                    String formattedAddress = (String) xpath.evaluate("formatted_address/text()", resultNode, XPathConstants.STRING);
                    geocodingResult.setFormattedAddress(formattedAddress);

                    Node locationNode = (Node) xpath.evaluate("geometry/location", resultNode, XPathConstants.NODE);
                    if (locationNode != null) {
                        String latString = (String) xpath.evaluate("lat/text()", locationNode, XPathConstants.STRING);
                        geocodingResult.setLatitude(latString.trim());
                        String lngString = (String) xpath.evaluate("lng/text()", locationNode, XPathConstants.STRING);
                        geocodingResult.setLongtitude(lngString.trim());
                    }

                    geocodingResults.add(geocodingResult);

                }
                response.setResults(geocodingResults);
            }
        } catch (XPathExpressionException e) {
            throw new IllegalArgumentException("Error in xpath evaluation", e);
        }

        return response;
    }
}
