package backbase.assignment.googlegeocodingrestapi;

import backbase.assignment.googlegeocodingrestapi.model.GeocodingResponse;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.ehcache.EhcacheConstants;
import org.apache.camel.model.rest.RestBindingMode;
import org.apache.camel.model.rest.RestParamType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class GoogleGeocodingRestRoute extends RouteBuilder {

    @Value("${host}")
    String host;

    @Value("${port}")
    String port;

    @Override
    public void configure() throws Exception {

        restConfiguration()
                .component("jetty")
                .host(host)
                .port(port)
                .bindingMode(RestBindingMode.json);

        rest("/geocode")
                .get()
                    .outType(GeocodingResponse.class)
                    .param().name("address").type(RestParamType.query).description("Address passed to rest api").endParam()
                    .to("direct:GetGeocode");

        from("ehcache://GeocodeCache")
            .to("mock:endpoint");

        from("direct:GetGeocode")
            .log("Getting geocode by address: ${header.address}")
            .setHeader(EhcacheConstants.ACTION, constant(EhcacheConstants.ACTION_GET))
            .setHeader(EhcacheConstants.KEY, header("address"))
            .to("ehcache://GeocodeCache")
            .choice()
                .when(header(EhcacheConstants.ACTION_HAS_RESULT).isNotEqualTo(true))
                    .log("Nothing found in cache, fetching XML data")
                    .process("GeocodeFetcher")
                    .setHeader(EhcacheConstants.ACTION, constant(EhcacheConstants.ACTION_PUT))
                    .setHeader(EhcacheConstants.KEY, header("address"))
                    .to("ehcache://GeocodeCache")
                .otherwise()
                    .log("Result found in cache, using XML data")
            .end()
            .bean(GeocodingResponseParser.class, "parse");
    }
}
