package backbase.assignment.googlegeocodingrestapi.model;

import java.util.List;

public class GeocodingResponse {

    String status;
    List<GeocodingResult> results;

    public GeocodingResponse() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<GeocodingResult> getResults() {
        return results;
    }

    public void setResults(List<GeocodingResult> results) {
        this.results = results;
    }

    @Override
    public String toString() {
        return "GeocodingResponse{" +
                "status='" + status + '\'' +
                ", results=" + results +
                '}';
    }

}

